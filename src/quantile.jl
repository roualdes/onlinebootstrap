# Adapted from
# https://github.com/MKL-NET/MKL.NET/blob/master/MKL.NET.Statistics/QuantileEstimator.cs
# under the license
# Copyright 2022 Anthony Lloyd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

struct OnlineBootQuantile{T <: AbstractFloat} <: AbstractOnlineBoot
    p::Vector{T}
    q::Vector{T}
    n::Vector{Int}
    Q::Matrix{T}
    N::Matrix{Int}
end

function OnlineBootQuantile(q, R)
    T = eltype(q)
    return OnlineBootQuantile([q],
                              zeros(T, R),
                              zeros(Int, R),
                              zeros(T, R, 4),
                              repeat([1 2 3 4], R))
end

# Nx => N[r, x+1]
# Q0 => Q[r, 1]
# Q1 => Q[r, 2]
# Qx => Q[r, x]

function accumulateob!(ob::OnlineBootQuantile, x, r)
    T = eltype(ob.p)

    ob.n[r] += 1

    if ob.n[r] > 5

        if x <= ob.Q[r, 3]

            ob.N[r, 4] += 1

            if x <= ob.q[r]

                ob.N[r, 3] += 1

                if x <= ob.Q[r, 2]

                    ob.N[r, 2] += 1

                    if x <= ob.Q[r, 1]

                        if x == ob.Q[r, 1]

                            ob.N[r, 1] += 1

                        else

                            ob.Q[r, 1] = x
                            ob.N[r, 1] = 1

                        end
                    end
                end
            end
        elseif x > ob.Q[r, 4]
            ob.Q[r, 4] = x
        end

        h = 0
        delta = zero(T)
        d1 = zero(T)
        d2 = zero(T)

        x = (ob.n[r] - 1) * ob.p[1] * 0.5 + 1 - ob.N[r, 2]

        if x >= one(T) && ob.N[r, 3] - ob.N[r, 2] > 1

            h = ob.N[r, 3] - ob.N[r, 2]
            delta = (ob.q[r] - ob.Q[r, 2]) / h

            d1 = PchipDerivative(ob.N[r, 2] - ob.N[r, 1],
                                 (ob.Q[r, 2] - ob.Q[r, 1]) / (ob.N[r, 2] - ob.N[r, 1]),
                                 h,
                                 delta)

            d2 = PchipDerivative(h,
                                 delta,
                                 ob.N[r, 4] - ob.N[r, 3],
                                 (ob.Q[r, 3] - ob.q[r]) / (ob.N[r, 4] - ob.N[r, 3]))

            ob.Q[r, 2] += HermiteInterpolationOne(h, delta, d1, d2)
            ob.N[r, 2] += 1

        elseif x <= -one(T) && ob.N[r, 2] - ob.N[r, 1] > 1

            h = ob.N[r, 2] - ob.N[r, 1]
            delta = (ob.Q[r, 2] - ob.Q[r, 1]) / h

            d1 = PchipDerivativeEnd(h,
                                    delta,
                                    ob.N[r, 3] - ob.N[r, 2],
                                    (ob.q[r] - ob.Q[r, 2]) / (ob.N[r, 3] - ob.N[r, 2]))

            d2 =  PchipDerivative(h,
                                  delta,
                                  ob.N[r, 3] - ob.N[r, 2],
                                  (ob.q[r] - ob.Q[r, 2]) / (ob.N[r, 3] - ob.N[r, 2]))

            ob.Q[r, 2] += HermiteInterpolationOne(h, -delta, -d2, -d1)
            ob.N[r, 2] -= 1
        end

        x = (ob.n[r] - 1) * ob.p[1] + 1 - ob.N[r, 3]

        if x >= one(T) && ob.N[r, 4] - ob.N[r, 3] > 1

            h = ob.N[r, 4] - ob.N[r, 3]
            delta = (ob.Q[r, 3] - ob.q[r]) / h

            d1 = PchipDerivative(ob.N[r, 3] - ob.N[r, 2],
                                 (ob.q[r] - ob.Q[r, 2]) / (ob.N[r, 3] - ob.N[r, 2]),
                                 h,
                                 delta)

            d2 = PchipDerivative(h,
                                 delta,
                                 ob.n[r] - ob.N[r, 4],
                                 (ob.Q[r, 4] - ob.Q[r, 3]) / (ob.n[r] - ob.N[r, 4]))

            ob.q[r] += HermiteInterpolationOne(h, delta, d1, d2)
            ob.N[r, 3] += 1

        elseif x <= -one(T) && ob.N[r, 3] - ob.N[r, 2] > 1

            h = ob.N[r, 3] - ob.N[r, 2]
            delta = (ob.q[r] - ob.Q[r, 2]) / h

            d1 = PchipDerivative(ob.N[r, 2] - ob.N[r, 1],
                                 (ob.Q[r, 2] - ob.Q[r, 1]) / (ob.N[r, 2] - ob.N[r, 1]),
                                 h,
                                 delta)

            d2 = PchipDerivative(h,
                                 delta,
                                 ob.N[r, 4] - ob.N[r, 3],
                                 (ob.Q[r, 3] - ob.q[r]) / (ob.N[r, 4] - ob.N[r, 3]))

            ob.q[r] += HermiteInterpolationOne(h, -delta, -d2, -d1)
            ob.N[r, 3] -= 1
        end

        x = (ob.n[r] - 1) * (1 + ob.p[1]) * 0.5 + 1 - ob.N[r, 4]

        if x >= one(T) && ob.n[r] - ob.N[r, 4] > 1

            h = ob.n[r] - ob.N[r, 4]
            delta = (ob.Q[r, 4] - ob.Q[r, 3]) / h

            d1 = PchipDerivative(ob.N[r, 4] - ob.N[r, 3],
                                 (ob.Q[r, 3] - ob.q[r]) / (ob.N[r, 4] - ob.N[r, 3]),
                                 h,
                                 delta)

            d2 = PchipDerivativeEnd(h,
                                    delta,
                                    ob.N[r, 4] - ob.N[r, 3],
                                    (ob.Q[r, 3] - ob.q[r]) / (ob.N[r, 4] - ob.N[r, 3]))

            ob.Q[r, 3] += HermiteInterpolationOne(h, delta, d1, d2)
            ob.N[r, 4] += 1

        elseif x <= -one(T) && ob.N[r, 4] - ob.N[r, 3] > 1

            h = ob.N[r, 4] - ob.N[r, 3]
            delta = (ob.Q[r, 3] - ob.q[r]) / h

            d1 = PchipDerivative(ob.N[r, 3] - ob.N[r, 2],
                                 (ob.q[r] - ob.Q[r, 2]) / (ob.N[r, 3] - ob.N[r, 2]),
                                 h,
                                 delta)

            d2 = PchipDerivative(h,
                                 delta,
                                 ob.n[r] - ob.N[r, 4],
                                 (ob.Q[r, 4] - ob.Q[r, 3]) / (ob.n[r] - ob.N[r, 4]))

            ob.Q[r, 3] += HermiteInterpolationOne(h, -delta, -d2, -d1)
            ob.N[r, 4] -= 1
        end

    elseif ob.n[r] == 5

        if x > ob.Q[r, 4]
            ob.Q[r, 1] = ob.Q[r, 2]
            ob.Q[r, 2] = ob.q[r]
            ob.q[r] = ob.Q[r, 3]
            ob.Q[r, 3] = ob.Q[r, 4]
            ob.Q[r, 4] = x
        elseif x > ob.Q[r, 3]
            ob.Q[r, 1] = ob.Q[r, 2]
            ob.Q[r, 2] =  ob.q[r]
            ob.q[r] = ob.Q[r, 3]
            ob.Q[r, 3] = x
        elseif x > ob.q[r]
            ob.Q[r, 1] = ob.Q[r, 2]
            ob.Q[r, 2] = ob.q[r]
            ob.q[r] = x
        elseif x > ob.Q[r, 2]
            ob.Q[r, 1] = ob.Q[r, 2]
            ob.Q[r, 2] = x
        else
            ob.Q[r, 1] = x
        end

    elseif ob.n[r] == 4

        if x < ob.Q[r, 2]
            ob.Q[r, 4] = ob.Q[r, 3]
            ob.Q[r, 3] = ob.q[r]
            ob.q[r] = ob.Q[r, 2]
            ob.Q[r, 2] = x
        elseif x < ob.q[r]
            ob.Q[r, 4] = ob.Q[r, 3]
            ob.Q[r, 3] = ob.q[r]
            ob.q[r] = x
        elseif x < ob.Q[r, 3]
            ob.Q[r, 4] = ob.Q[r, 3]
            ob.Q[r, 3] = x
        else
            ob.Q[r, 4] = x
        end

    elseif ob.n[r] == 3

        if x < ob.Q[r, 2]
            ob.Q[r, 3] = ob.q[r]
            ob.q[r] = ob.Q[r, 2]
            ob.Q[r, 2] = x
        elseif x < ob.q[r]
            ob.Q[r, 3] = ob.q[r]
            ob.q[r] = x
        else
            ob.Q[r, 3] = x
        end

    elseif ob.n[r] == 2

        if x > ob.q[r]
            ob.Q[r, 2] = ob.q[r]
            ob.q[r] = x
        else
            ob.Q[r, 2] = x
        end
    else
        ob.q[r] = x
    end
end

function PchipDerivative(h1, delta1, h2, delta2)
    return (h1 + h2) * 3 * delta1 * delta2 / ((h1 * 2 + h2) * delta1 + (h2 * 2 + h1) * delta2)
end

function PchipDerivativeEnd(h1, delta1, h2, delta2)
    d = (delta1 - delta2) * h1 / (h1 + h2) + delta1
    return d < zero(d) ? zero(d) : d
end

function HermiteInterpolationOne(h1, delta1, d1, d2)
    return ((d1 + d2 - delta1 * 2) / h1 + delta1 * 3 - d1 * 2 - d2) / h1 + d1
end
