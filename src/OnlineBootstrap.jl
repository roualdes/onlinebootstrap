module OnlineBootstrap

abstract type AbstractOnlineBoot end

include("bootstrap.jl")
include("poisson.jl")

include("mean.jl")
include("var.jl")
include("std.jl")
include("quantile.jl")

export
    boot,
    boot!,
    randp,
    OnlineBootMean,
    OnlineBootVar,
    OnlineBootStd,
    OnlineBootQuantile,
    accumulateob!

end
