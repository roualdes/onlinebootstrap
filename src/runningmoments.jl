# Adapted from
# https://github.com/tensorflow/probability/blob/76ff71ba27a5a035fa6220e6132744ac89a56fdf/spinoffs/fun_mc/fun_mc/fun_mc_lib.py#L2127
# Copyright 2021 The TensorFlow Probability Authors.
# Licensed under the Apache License, Version 2.0
# which itself was adapted from
# https://notmatthancock.github.io/2017/03/23/simple-batch-stat-updates.html

# only works for vector mass matrix
struct RunningMoments{T<:AbstractFloat}
    m::VecOrMat{T}
    v::VecOrMat{T}
    n::Vector{Int}
end

function RunningMoments(T, d)
    return RunningMoments(zeros(T, d),
                         zeros(T, d),
                         zeros(Int, 1))
end

RunningMoments(d) = RunningMoments(Float64, d)

"""
    accumulatemoments!(rm::RunningMoments, x::AbstractMatrix)

TODO
"""
function accumulatemoments!(rm::RunningMoments, xs)
    m = xs
    v = zero(xs)

    m .-= rm.m
    v .-= rm.v
    w = 1 / (rm.n[1] + 1)

    @. rm.m += w * m
    @. rm.v += w * v + w * (1 - w) * m ^ 2
    rm.n[1] += 1
end

function reset!(rm::RunningMoments)
    rm.n[1] = zero(rm.n[1])
    rm.m .= zero(rm.m)
    rm.v .= zero(rm.v)
end
