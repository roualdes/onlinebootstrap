struct OnlineBootStd{T <: AbstractFloat} <: AbstractOnlineBoot
    m::Vector{T}
    v::Vector{T}
    s::Vector{T}
    n::Vector{Int}
end

function OnlineBootStd(T, R)
    return OnlineBootStd(zeros(T, R),
                         zeros(T, R),
                         zeros(T, R),
                         zeros(Int, R))
end

OnlineBootStd(R) = OnlineBootStd(Float64, R)

function accumulateob!(ob::OnlineBootStd, x, r)
    m = x
    v = zero(x)

    m -= ob.m[r]
    v -= ob.v[r]

    ob.n[r] += 1
    w = 1 / ob.n[r]

    ob.m[r] += w * m
    ob.v[r] += w * v + w * (1 - w) * m ^ 2
    ob.s[r] = sqrt(ob.v[r])
end
