using OnlineBootstrap
using Statistics
using Random
using Test


@testset "OnlineBootstrap.jl" begin

    @testset "accumulators" begin

        N = 10000
        D = 10
        x = randn(N)
        R = 1

        obm = OnlineBootMean(R)
        obv = OnlineBootVar(R)
        obs = OnlineBootStd(R)
        obqA = OnlineBootQuantile(0.1, R)
        obqB = OnlineBootQuantile(0.25, R)
        obqC = OnlineBootQuantile(0.33, R)
        obqD = OnlineBootQuantile(0.420, R)
        obqE = OnlineBootQuantile(0.5, R)
        obqF = OnlineBootQuantile(0.75, R)
        obqG = OnlineBootQuantile(0.89, R)
        obqH = OnlineBootQuantile(0.95, R)


        for n in eachindex(x)
            accumulateob!(obm, x[n], 1)
            accumulateob!(obv, x[n], 1)
            accumulateob!(obs, x[n], 1)
            accumulateob!(obqA, x[n], 1)
            accumulateob!(obqB, x[n], 1)
            accumulateob!(obqC, x[n], 1)
            accumulateob!(obqD, x[n], 1)
            accumulateob!(obqE, x[n], 1)
            accumulateob!(obqF, x[n], 1)
            accumulateob!(obqG, x[n], 1)
            accumulateob!(obqH, x[n], 1)
        end

        @test isapprox(obm.m[1], mean(x))
        @test obm.n[1] == N

        @test isapprox(obv.v[1], var(x, corrected = false))
        @test obv.n[1] == N

        @test isapprox(obs.s[1], std(x, corrected = false))
        @test obs.n[1] == N

        @test obs.n[1] == N
        @test isapprox(obqA.q[1], -1.2815515655446004, atol = 5e-2)
        @test isapprox(obqB.q[1], -0.6744897501960818, atol = 5e-2)
        @test isapprox(obqC.q[1], -0.4399131656732337, atol = 5e-2)
        @test isapprox(obqD.q[1], -0.20189347914185085, atol = 5e-2)
        @test isapprox(obqE.q[1], 0.0, atol = 5e-2)
        @test isapprox(obqF.q[1], 0.6744897501960818, atol = 5e-2)
        @test isapprox(obqG.q[1], 1.22652812003661, atol = 5e-2)
        @test isapprox(obqH.q[1], 1.6448536269514717, atol = 5e-2)

    end

    @testset "bootstrap" begin

        Random.seed!(204)
        R = 31
        N = 10000
        y = randn(N)

        means = boot(y, mean, R)

        @test isapprox(std(means), 1 / sqrt(N), atol = 1e-2)

        obm = OnlineBootMean(R)

        for n in 1:N
            boot!(obm, y[n])
        end

        @test isapprox(std(obm.m), 1 / sqrt(N), atol = 1e-2)

        obq = OnlineBootQuantile(0.5, R)

        for n in 1:N
            boot!(obq, y[n])
        end

        @test isapprox(std(obq.q), 1 / sqrt(4N * 0.3989422804014327 ^ 2), atol = 1e-2)

    end

end
